from osgeo import gdal, osr
import argparse
from math import radians, sin, cos, asin, sqrt
import pyproj
from PIL import Image
import numpy as np


def haversine(gps1: tuple, gps2: tuple) -> float:
    """Returns the distance in meters between two GPS coordinates
    Args:
        two tuples containing latitude and longitude"""
    
    lat1, lon1 = gps1
    lat2, lon2 = gps2

    # convert decimal degrees to radians
    lon1, lat1, lon2, lat2 = map(radians, [lon1, lat1, lon2, lat2])
    # haversine formula
    dlon = lon2 - lon1
    dlat = lat2 - lat1
    a = sin(dlat / 2) ** 2 + cos(lat1) * cos(lat2) * sin(dlon / 2) ** 2
    c = 2 * asin(sqrt(a))
    m = 6371000 * c
    return m

def count_pixels(geo_tiff_path: str) -> dict:
    """Returns dict of count of pixels per a unique color
    Args:
        geo_tiff_path (str): the image to count the number of pixels of
    Returns:
        a key-value pairing of the rgb color value and the number of times the color was present in the image
    """

    color_count = {}
    with Image.open(geo_tiff_path) as image:
        width, height = image.size
        rgb_image = image.convert('RGB')

        # iterate through each pixel in the image and keep a count per unique color
        for x in range(width):
            for y in range(height):
                rgb = rgb_image.getpixel((x, y))

                if rgb in color_count:
                    color_count[rgb] += 1
                else:
                    color_count[rgb] = 1

    return color_count

def width_height_in_meters(geo_tiff_path: str) -> tuple:
    """Returns width and height of the image in meters
    Args:
        geo_tiff_path (str): the image to count the number of pixels of       
    """

    geo_tiff = gdal.Open(geo_tiff_path)

    raster_band = geo_tiff.GetRasterBand(1)
    img = raster_band.ReadAsArray()
    [height, width] = np.shape(img)

    geo_transform = geo_tiff.GetGeoTransform()
    cols = geo_tiff.RasterXSize
    rows = geo_tiff.RasterYSize

    left_top_point = geo_transform[0], geo_transform[3]
    right_top_point = geo_transform[0] + cols * geo_transform[1], \
                      geo_transform[3]
    left_bottom_point = geo_transform[0], geo_transform[3] + rows * geo_transform[5]
    right_bottom_point = geo_transform[0] + cols * geo_transform[1], \
                         geo_transform[3] + rows * geo_transform[5]

    projection = osr.SpatialReference(wkt=geo_tiff.GetProjection())
    input_system = projection.GetAttrValue("AUTHORITY", 1)

    if input_system == None:
        input_system = "3857"
    transform2wgs = pyproj.Transformer.from_crs("EPSG:" + input_system, "EPSG:4326")

    lon, lat = transform2wgs.transform(left_top_point[0], left_top_point[1])
    left_top_point = lon, lat
    lon, lat = transform2wgs.transform(right_top_point[0], right_top_point[1])
    right_top_point = lon, lat
    lon, lat = transform2wgs.transform(right_bottom_point[0], right_bottom_point[1])
    right_bottom_point = lon, lat
    lon, lat = transform2wgs.transform(left_bottom_point[0], left_bottom_point[1])
    left_bottom_point = lon, lat

    width = haversine(left_top_point, right_top_point)
    height = haversine(left_top_point, left_bottom_point)

    return width, height

def area_values(geo_tiff_path: str) -> dict:
    """Returns dict of the area of each unique color
    Args:
        geo_tiff_path (str): the image to count the number of pixels of
    Returns:
        a key-value pairing of the rgb color value and the area of this color
    """

    geo_tiff = gdal.Open(geo_tiff_path)

    color_count = count_pixels(geo_tiff_path) # dict {(x,x,x): y,...}
    width, height = width_height_in_meters(geo_tiff_path) # meters
    cols = geo_tiff.RasterXSize # pixels
    rows = geo_tiff.RasterYSize # pixels

    pixel_width = width / cols
    pixel_height = height / rows
    pixel_area = pixel_width * pixel_height

    for color in color_count:
        color_count[color] *= pixel_area
    
    print("The area of unique flowers in square meters")
    for key in color_count:
        print(f"{key}: {color_count[key]}")

    return color_count


if __name__ == "__main__":

    parser = argparse.ArgumentParser(
        description='Converter left top point geotiff file '
                    'from different coordinate systems to Lat Lon')

    parser.add_argument('-i', '--input_file', type=str,
                        help='path to geo tiff file')

    try:
        args = parser.parse_args()
        area_values(args.input_file)
    except ImportError:
        parser.print_help()
